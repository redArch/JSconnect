const BLOBCOUNT = 5;
const BLOBSPEED = 5;
const BLOBSIZERANDOMNESS = 1;
const BLOBSIZE = 300;

var blobs = [];

// create the blobs
function initBlobs()
{
	for (var i = 0; i < BLOBCOUNT; i++)
	{
		// creates blob object
		blobs[i] = new Object();
		
		// scatter blobs across canvas
		blobs[i].x = Math.random() * canvas.width;
		blobs[i].y = Math.random() * canvas.height;
		
		// sets random speed
		blobs[i].xSpeed = (Math.random() - 0.5) * BLOBSPEED;
		blobs[i].ySpeed = (Math.random() - 0.5) * BLOBSPEED;
		
		blobs[i].size = (Math.random() + BLOBSIZERANDOMNESS) * BLOBSIZE;
	}
}

// blobs main, called every frame
function blobsMain()
{
	// move all the blobs
	// and draw them
	for (var i in blobs)
	{
		blobs[i] = moveBlob(blobs[i]);
		drawBlob(blobs[i]);
	}
}

// takes a blob with x and y coordinates and draws a blob there
function drawBlob(blob)
{
	// create a circular gradient
	var gradient = drawSpace.createRadialGradient(blob.x, blob.y, 0, blob.x, blob.y, blob.size);

	// create a bluish-green circle that fades out
	gradient.addColorStop(0, 'rgba(15, 88, 88, 1)');
	gradient.addColorStop(1, 'rgba(15, 88, 88, 0)');

	// create a circular path
	drawSpace.beginPath();
	drawSpace.arc(blob.x, blob.y, blob.size, 0, 2 * Math.PI);

	// fill path using gradient
	drawSpace.fillStyle = gradient;
	drawSpace.fill();
}

function moveBlob(blob)
{
	// move blob using it's speed
	blob.x += blob.xSpeed;
	blob.y += blob.ySpeed;
	
	// slightly change blob speed
	blob.xSpeed += (Math.random() - 0.5) * (BLOBSPEED / 40);
	blob.ySpeed += (Math.random() - 0.5) * (BLOBSPEED / 40);
	
	// change speeds when blob goes off canvas
	if (blob.x < -20) blob.xSpeed = BLOBSPEED / 4;
	else if (blob.x > window.innerWidth + 20) blob.xSpeed = -BLOBSPEED / 4;
	
	if (blob.y < -20) blob.ySpeed = BLOBSPEED / 4;
	else if (blob.y > window.innerHeight + 20) blob.ySpeed = -BLOBSPEED / 4;
	
	return blob;
}
