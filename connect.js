const CONNECTIONLENGTH = 20;
const POINTCOUNT = 200;
const POINTRADIUS = 3;
const LINEWIDTH = 0.5;
const POINTSPEED = 1.6;

// initialize points and their inital locations
var points = [];

// track delay between resize events
var timeout = false
var delta = 25

// rescatter points on resize with a delay of 25ms which
// prevents initConnect from being called excessively
window.addEventListener('resize', () =>
{
	currentTime = new Date();

	if (timeout == false)
	{
		timeout = true;
		setTimeout(() => {initConnect(); timeout = false}, delta);
	}
});

// create points and scatter them across the canvas
function initConnect()
{
	for (var i = 0; i < POINTCOUNT; i++)
	{
		// creates the point object
		points[i] = new Object();

		// sets random position
		points[i].x = Math.random() * canvas.width;
		points[i].y = Math.random() * canvas.height;

		// sets random speed
		points[i].xSpeed = (Math.random() - 0.5) * POINTSPEED;
		points[i].ySpeed = (Math.random() - 0.5) * POINTSPEED;
	}
}

// main function which is called every frame
function connectMain()
{
	// creates an array of point pairs that are within CONNECTIONLENGTH
	var connections = connectPairs();

	// go through all connection pairs and draw lines between them
	for (var i in connections)
	{
		drawLine(points[connections[i][0]], points[connections[i][1]]);
	}

	// move point and then draw it
	for (var i in points)
	{
		points[i] = movePoint(points[i]);
		drawPoint(points[i]);
	}
}

// takes two points with x and y coordinates and draws a line between them
function drawLine(point1, point2)
{
	// create drawspace as path
	drawSpace.beginPath();

	// move draw space to point1 and then create a line to point2
	drawSpace.moveTo(point1.x, point1.y);
	drawSpace.lineTo(point2.x, point2.y);

	// set line style
	drawSpace.strokeStyle = 'rgba(128, 255, 255, 0.25)';

	// linewidth is 0.5px
	drawSpace.lineWidth = LINEWIDTH;

	// actually draw the line
	drawSpace.stroke();
}

// takes a point with x and y coordinates and draws a point there
function drawPoint(point)
{
	// create a circular gradient
	var gradient = drawSpace.createRadialGradient(point.x, point.y, 0, point.x, point.y, POINTRADIUS);

	// create a bluish-green circle that fades out
	gradient.addColorStop(0, 'rgba(66, 255, 255, 1)');
	gradient.addColorStop(1, 'rgba(66, 255, 255, 0)');

	// create a circular path
	drawSpace.beginPath();
	drawSpace.arc(point.x, point.y, POINTRADIUS, 0, 2 * Math.PI);

	// fill path using gradient
	drawSpace.fillStyle = gradient;
	drawSpace.fill();
}

// takes a point with x, y, coordinates and speed then returns it at a new location
function movePoint(point)
{
	// move point using it's speed
	point.x += point.xSpeed;
	point.y += point.ySpeed;

	// slightly change point speed
	point.xSpeed += (Math.random() - 0.5) * (POINTSPEED / 40);
	point.ySpeed += (Math.random() - 0.5) * (POINTSPEED / 40);

	// change speeds when point goes off canvas
	if (point.x < -20) point.xSpeed = POINTSPEED / 4;
	else if (point.x > window.innerWidth + 20) point.xSpeed = -POINTSPEED / 4;

	if (point.y < -20) point.ySpeed = POINTSPEED / 4;
	else if (point.y > window.innerHeight + 20) point.ySpeed = -POINTSPEED / 4;

	return point;
}

// return array of pairs of points that are within CONNECTIONLENGTH
function connectPairs()
{
	var connectionLength = CONNECTIONLENGTH / (POINTCOUNT / Math.sqrt(window.innerWidth * window.innerHeight));

	var connections = [];

	// check all points agianst all other points
	// but does not recheck previous combinations
	for (var i = 0; i < points.length - 1; i++)
	{
		for (var j = i + 1; j < points.length; j++)
		{
			if (distance(points[i], points[j]) < connectionLength) connections.push([i, j]);
		}
	}

	return connections;
}

// returns distance between two points
function distance(point1, point2)
{
	return Math.sqrt(Math.pow(Math.abs(point1.x - point2.x), 2) + Math.pow(Math.abs(point1.y - point2.y), 2));
}
