// vars for drawing

var canvas, drawspace;

window.onload = function()
{
	// get html canvas and assign drawspace
	canvas = document.getElementById("canvas");
	drawSpace = canvas.getContext("2d");

	// set canvas dimensions to use the entire page
	canvas.setAttribute("width", window.innerWidth);
	canvas.setAttribute("height", window.innerHeight);

	initConnect();
	initBlobs();

	requestAnimationFrame(mainMain);
}

function mainMain()
{
	// change canvas dimensions in case browser is resized
	// will also clear previously drawn objects
	canvas.setAttribute("width", window.innerWidth);
	canvas.setAttribute("height", window.innerHeight);

	// call the blob.js main function
	blobsMain();

	// call the connect.js main function
	connectMain();

	// repeat main function as animation
	requestAnimationFrame(mainMain);
}
